import math

values = [int(x) for x in open('file.txt').read().split()]


def max_number(array):
    return max(array)


def min_number(array):
    return min(array)


def summa(array):
    return sum(array)


def prod(array):
    return math.prod(array)


