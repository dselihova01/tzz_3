import pytest
from random import randint
from main import max_number, min_number, summa, prod, values


# вспомогательные функции
def count_summa(array):
    res = 0
    for i in array:
        res += i
    return res


def count_prod(array):
    res = 1
    for i in array:
        res *= i
    return res


def random_array(length, low, high):
    result = []
    for i in range(length):
        result.append(randint(low, high))
    return result


def search_max(array):
    max_num = array[0]
    for i in array:
        if i > max_num:
            max_num = i
    return max_num


def search_min(array):
    min_num = array[0]
    for i in array:
        if i < min_num:
            min_num = i
    return min_num


# Тесты проверяющие правильность работы функци
def test_min_number_1():
    array = [1, 2, 3, 4]
    assert search_min(array) == min_number(array)


def test_max_number_1():
    array = [1, 2, 3, 4]
    assert search_max(array) == max_number(array)


def test_summa_1():
    array = [1, 2, 3, 4]
    assert count_summa(array) == summa(array)


def test_prod_1():
    array = [1, 2, 3, 4]
    assert count_prod(array) == prod(array)


# Тесты проверяющие скорость работы программы при увеличении входных данных
def test_max_number_2():
    array = sorted(random_array(100000, -10000, 10000))
    assert search_max(array) == max_number(array)


def test_min_number_2():
    array = sorted(random_array(100000, -10000, 10000))
    assert search_min(array) == min_number(array)


def test_summa_2():
    array = sorted(random_array(100000, -10000, 10000))
    assert count_summa(array) == summa(array)


def test_prod_2():
    array = sorted(random_array(100000, -10000, 10000))
    assert count_prod(array) == prod(array)


# Дополнительный тест
def test_prod_is_zero():
    assert prod(values) != 0
